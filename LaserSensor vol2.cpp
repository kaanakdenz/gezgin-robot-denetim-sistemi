#include "LaserSensor.h"
#include <iostream>

LaserSensor::LaserSensor() {}
LaserSensor::~LaserSensor() {}


float LaserSensor::getRange(int index) const
{
	if (0 <= index <= 16) {
		return this->ranges[index];
	}
	else return -1;
}

/**
* \brief updateSensor function of LaserSensor, this function updates sensor values by demanded vales's array
*\param range[]
*/
void  LaserSensor::updateSensor(float range[])
{
	for (int i = 0; i < 181; i++) {
		this->ranges[i] = range[i];
	}
}

/**
* \brief getMax function of LaserSensor, this function finds the maximum range belong sensors, returns maximum range
and pass index of maximum to demanded index value
*\param &index
*/
float LaserSensor::getMax(int &index)
{
	int i = 0;
	float max = this->ranges[i];
	for (int i = 0; i<16; i++)
	{
		if (this->ranges[i] > max) {
			max = this->ranges[i];
			index = i;
		}
	}
	return max;
}

/**
* \brief getMin function of LaserSensor, this function finds the minimum range belong sensors, returns minimum range
and pass index of minimum to demanded index value
*\param &index
*/
float LaserSensor::getMin(int &index)
{
	int i = 0;
	float min = this->ranges[i];
	for (int i = 0; i<16; i++)
	{
		if (min>this->ranges[i]) {
			min = this->ranges[i];
			index = i;
		}
	}
	return min;
}

/**
* \brief updateSensor function of SonarSensor, this function updates sensor values by demanded vales's array
*\param range[]
*/
float LaserSensor::operator[](int i)
{
	if (0 <= i <= 16) {
		return ranges[i];
	}
	else return -1;
}

/**
* \brief getAngle function of SonarSensor, this function finds angle of demanded sensor
*\param index
*/
float LaserSensor::getAngle(int index)
{
	if (index == 0 || index == 8) {
		return 90;
	}else if (index == 1 || index == 9) {
		return 50;
	}
	else if (index == 2 || index == 10) {
		return 30;
	}
	else if (index == 3 || index == 11) {
		return 10;
	}
	else if (index == 4 || index == 12) {
		return -10;
	}
	else if (index == 5 || index == 13) {
		return -30;
	}
	else if (index == 6 || index == 14) {
		return -50;
	}
	else if (index == 7 || index == 15) {
		return -90;
	}
	else {
		return -1;
	}
}

/**
* \brief getClosestAngle function of LaserSensor, this function finds the minimum range between demanded angles 
returns minimum angle and pass index of minimum angle to &angle
*\param startAngle, endAngle, &angle
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float &angle)
{

	float minRange = this->ranges[int(startAngle)];
	for (float i = startAngle; i<endAngle; i++)
	{
		if (minRange>this->ranges[int(i)]) {
			minRange = this->ranges[int(i)];
			angle = i;
		}
	}
	return minRange;
}