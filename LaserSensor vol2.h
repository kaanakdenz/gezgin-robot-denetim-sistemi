#pragma once
#include "RangeSensor.h"
#include <iostream>

/**
* @file SonarSensor.h
* @Author �rfan AKARSU
* @brief SonarSensor for recording and showing ranges
*/
class LaserSensor:public RangeSensor
{	
public:
	LaserSensor();
	~LaserSensor();

	float getRange(int) const;
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float[]);
	float operator[](int);
	float getAngle(int);
	float getClosestRange(float, float, float&);
};
