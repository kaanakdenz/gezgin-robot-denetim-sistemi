#include "LaserSensor.h"
#include <iostream>

LaserSensor::LaserSensor() {}
LaserSensor::~LaserSensor() {}


float LaserSensor::getRange(int index) const
{
	if (0 <= index <= 16) {
		return this->ranges[index];
	}
	else return -1;
}

void  LaserSensor::updateSensor(float range[])
{
	for (int i = 0; i < 16; i++) {
		this->ranges[i] = range[i];
	}
}

float LaserSensor::getMax(int &index)
{
	int i = 0;
	float max = this->ranges[i];
	for (int i = 0; i<16; i++)
	{
		if (this->ranges[i] > max) {
			max = this->ranges[i];
			index = i;
		}
	}
	return max;
}

float LaserSensor::getMin(int &index)
{
	int i = 0;
	float min = this->ranges[i];
	for (int i = 0; i<16; i++)
	{
		if (min>this->ranges[i]) {
			min = this->ranges[i];
			index = i;
		}
	}
	return min;
}

float LaserSensor::operator[](int i)
{
	if (0 <= i <= 16) {
		return ranges[i];
	}
	else return -1;
}

float LaserSensor::getAngle(int index)
{
	if (index == 0 || index == 8) {
		return 90;
	}
	else if (index == 1 || index == 9) {
		return 50;
	}
	else if (index == 2 || index == 10) {
		return 30;
	}
	else if (index == 3 || index == 11) {
		return 10;
	}
	else if (index == 4 || index == 12) {
		return -10;
	}
	else if (index == 5 || index == 13) {
		return -30;
	}
	else if (index == 6 || index == 14) {
		return -50;
	}
	else if (index == 7 || index == 15) {
		return -90;
	}
	else {
		return -1;
	}
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float &angle)
{

	float minRange = this->ranges[int(startAngle)];
	for (float i = startAngle; i<endAngle; i++)
	{
		if (minRange>this->ranges[int(i)]) {
			minRange = this->ranges[int(i)];
			angle = i;
		}
	}
	return minRange;
}