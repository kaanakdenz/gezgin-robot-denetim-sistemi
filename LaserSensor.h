#pragma once
#include "PioneerRobotAPI.h"
#include <iostream>

/**
* @file SonarSensor.h
* @Author �rfan AKARSU
* @brief SonarSensor for recording and showing ranges
*/
class LaserSensor
{
private:
	float ranges[181];
	PioneerRobotAPI *robotAPI;;
public:
	LaserSensor();
	~LaserSensor();
	//! getRange function of LaserSensor, this function finds the range of demanded sensor
	float getRange(int) const;
	//!getMax function of LaserSensor, this function finds the maximum range belong sensors, returns maximum range
	//!and pass index of maximum to demanded index value
	float getMax(int&);
	//!getMin function of LaserSensor, this function finds the minimum range belong sensors, returns minimum range
	//!and pass index of minimum to demanded index value
	float getMin(int&);
	//!updateSensor function of LaserSensor, this function updates sensor values by demanded vales's array
	void updateSensor(float[]);
	//!operator[] function of SonarSensor, this function overrides [] operator for returning value of demanded sensor
	float operator[](int);
	//!getAngle function of SonarSensor, this function finds angle of demanded sensor
	float getAngle(int);
	//!getClosestAngle function of LaserSensor, this function finds the minimum range between demanded angles
	//!returns minimum angle and pass index of minimum angle to &angle
	float getClosestRange(float, float, float&);
};
