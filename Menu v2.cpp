#include<iostream>
#include"PioneerRobotInterface.h"
#include"RobotControl.h"

/**
* @file Menu.cpp
* @Author Kaan Akdeniz , Cenkay Basaran
* @date 26.12.2018
* @brief This file using for control of robot position.
*/

using namespace std;

void MotionMenu(RobotInterface* controller,RobotControl*,int);
void ConnectionMenu(PioneerRobotAPI* robot);
void SensorMenu(RangeSensor* sensorL, RangeSensor* sensorS);
void LaserSensorMenu(RangeSensor* Lsensor);
void SonarSensorMenu(RangeSensor* Ssensor);


int main()
{
	PioneerRobotAPI *robbot = new PioneerRobotAPI();
	RobotControl *robot = new RobotControl();
	RobotInterface *rob = new PioneerRobotInterface(robbot);
	RangeSensor *Lsensor = new LaserSensor();
	RangeSensor *Ssensor = new SonarSensor();

	unsigned int password;
	cout << "Enter your password to operate the robot" << endl;
	cin >> password;
	robot->openAccess(password);

	int choice;
	do {
		cout << "Main Menu -- Choose One" << endl;
		cout << "1 - Connection" << endl;
		cout << "2 - Motion" << endl;
		cout << "3 - Sensor" << endl;
		cout << "4 - Quit" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			ConnectionMenu(robbot);
			break;
		case 2:
			MotionMenu(rob,robot,password);
			break;
		case 3:
			SensorMenu(Lsensor,Ssensor);
			break;
		case 4:
			break;
		default:
			cout << "Invalid Input!";
			break;
		}
			   			

	} while (choice!=4);
	
}

void MotionMenu(RobotInterface* controller,RobotControl* cont, int password)
{
	int choice;
	do {

		system("cls");
		cout << "1-Forward" << endl;
		cout << "2-Backward" << endl;
		cout << "3-Turn Right" << endl;
		cout << "4-Turn Left" << endl;
		cout << "5-Stop Move" << endl;
		cout << "6-Stop Turn" << endl;
		cout << "7-Clear Path " << endl;
		cout << "8-Add to Path " << endl;
		cout << "9-Record Path To File " << endl;
		cout << "10-Close Access " << endl;
		cout << "11-Exit" << endl;
		cin >> choice;

		switch (choice)
		{

		case 1:
			controller->forward(5000);
			Sleep(1000);
			break;
		case 2:
			controller->backward(5000);
			Sleep(1000);
			break;
		case 3:
			controller->turnRight();
			break;
		case 4:
			controller->turnLeft();
			Sleep(1000);
			break;
		case 5:
			controller->stopMove();
			break;
		case 6:
			controller->stopTurn();
			break;
		case 7:
			cont->clearPath();
			return;
			break;
		case 8:
			cont->addToPath();
			break;
		case 9:
			cont->recordPathtoFile();
			break;
		case 10:
			cout << "Sifre giriniz: " << endl;
			cin >> password;
			cont->closeAccess(password);
			break;
		case 11:
			break;
		default:
			cout << "Invalid input!" << endl;
			break;
		}
					   
	} while (choice != 11);


}

void ConnectionMenu(PioneerRobotAPI* robot) {

	int choice;
	do {
		system("cls");
		cout << "Connection Menu -- Choose Menu" << endl;
		cout << "1 - Connect Robot"<<endl;
		cout << "2 - Disconnect Robot" << endl;
		cout << "3 - Back" << endl;

		cin >> choice;
		switch (choice)
		{
		case 1:
			robot->connect();
			break;
		case 2:
			robot->disconnect();
			break;
		case 3:
			break;
		default:
			cout << "Invalid Input!";
			break;
		}

	} while (choice!=3);



}

void SensorMenu(RangeSensor* sensorL, RangeSensor* sensorS) {

	int choice;
	do {

		system("cls");
		cout << "Sensor Menu -- Choose One" << endl;
		cout << "1-Laser Sensor" << endl;
		cout << "2-Sonar Sensor" << endl;
		cout << "3-Back" << endl;
	
		cin >> choice;

		switch (choice)
		{

		case 1:
			LaserSensorMenu(sensorL);
			break;
		case 2:
			SonarSensorMenu(sensorS);
			Sleep(1000);
			break;
		case 3:
			break;	
		default:
			cout << "Invalid input!" << endl;
			break;
		}

	} while (choice != 3);

}

void LaserSensorMenu(RangeSensor* sensor) {
	float ranges[181] = { 1,2,3,4,5,6,7,8,15,10,11,12,13,14,0,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,
		1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,12 };
	sensor->updateSensor(ranges);
	int choice, a=5, b=6;


	do {
		cout << "Laser Sensor Menu -- Choose One" << endl;
		cout << "1 - Get Max Value" << endl;
		cout << "2 - Get Min Value" << endl;
		cout << "3 - Get Range" << endl;
		cout << "4 - Get Angle" << endl;
		cout << "5 - Back" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout<< "Maximum value is:"<<sensor->getMax(a)<<endl;
			break;
		case 2:
			cout << "Minimum value is:" << sensor->getMax(b) << endl;
			break;
		case 3:
			cout << "Range of demanded sensor is:" << sensor->getRange(5) << endl;
			break;
		case 4:
			cout << "Angle of demanded sensor is:" << sensor->getAngle(3) << endl;
			break;
		case 5:
			break;
		default:
			cout << "Invalid Input!";
			break;
		}
			   

	} while (choice != 5);


}

void SonarSensorMenu(RangeSensor* sensor) {
	
	int choice, a = 5, b = 6;
	float ranges[16] = { 3,2,1,4,5,6,7,8,15,3,10,11,12,13,14,8 };
	sensor->updateSensor(ranges);

	do {
		cout << "Sonar Sensor Menu -- Choose One" << endl;
		cout << "1 - Get Max Value" << endl;
		cout << "2 - Get Min Value" << endl;
		cout << "3 - Get Range" << endl;
		cout << "4 - Get Angle" << endl;
		cout << "5 - Back" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Maximum value is:" << sensor->getMax(a) << endl;
			break;
		case 2:
			cout << "Minimum value is:" << sensor->getMax(b) << endl;
			break;
		case 3:
			cout << "Range of demanded sensor is:" << sensor->getRange(5) << endl;
			break;
		case 4:
			cout << "Angle of demanded sensor is:" << sensor->getAngle(3) << endl;
			break;
		case 5:
			break;
		default:
			cout << "Invalid Input!";
			break;
		}


	} while (choice != 5);




}