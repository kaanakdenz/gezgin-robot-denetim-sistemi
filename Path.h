#pragma once
#include"Node.h"
#include"pose.h"
#include<iostream>
class Path
{
private:
	Node* tail=NULL;
	Node* head;
	int number;

public:
	Path();
	~Path();
	void addPos(Pose Pose);
	void print();
	Pose &operator[](int a) const;
	Pose getPose(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose Pose);
	void operator>>(Pose a);
	bool operator<<(Path a);
	
};

