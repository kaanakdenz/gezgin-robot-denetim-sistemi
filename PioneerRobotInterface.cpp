#include "PioneerRobotInterface.h"
/**
* @file PioneerRobotInterface.cpp
* @Author Kaan Akdeniz 152120161031
* @date 26.12.2018
* @brief This file using for control of robot.
*/

void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void PioneerRobotInterface::forward(float speed)
{
	robotAPI->moveRobot(speed);
}

void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(-speed);
}

void PioneerRobotInterface::print()
{
	cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
}

Pose PioneerRobotInterface::getPose()
{
	return *position;
}

void PioneerRobotInterface::setPose(Pose pos)
{
	position->setX(pos.getX());
	position->setY(pos.getY());
	position->setTh(pos.getTh());
}

void PioneerRobotInterface::stopTurn()
{
	robotAPI->stopRobot();
}

void PioneerRobotInterface::stopMove()
{
	robotAPI->stopRobot();
}

void PioneerRobotInterface::updateSensors()
{
	float ranges[181] = { 1,2,3,4,5,6,7,8,15,10,11,12,13,14,0,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,
		1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,12 };
	sensor->updateSensor(ranges);
}
