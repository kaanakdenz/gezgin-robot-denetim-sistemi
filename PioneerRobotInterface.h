#pragma once
#include"RobotInterface.h"
#include"RangeSensor.h"

/**
* @file PioneerRobotInterface.h
* @Author Kaan Akdeniz 152120161031
* @date 26.12.2018
* @brief This file using for control of robot.
*/

class PioneerRobotInterface:public RobotInterface
{
private:
	PioneerRobotAPI* robotAPI;
	RangeSensor *sensor;
public:
	//! Constructor
	PioneerRobotInterface(PioneerRobotAPI* robot) {
		robotAPI = robot;
	};
	//! Destructor
	~PioneerRobotInterface() {};
	//! Move robot to left.
	void turnLeft();
	//! Move robot to right.
	void turnRight();
	//! Move robot to forward with speed paramater.
	void forward(float speed);
	//! Move robot to backward with speed parameter.
	void backward(float speed);
	//! Print robot position.
	void print();
	//! Get robot position.
	Pose getPose();
	//! Set robot position.
	void setPose(Pose);
	//! Stop turning.
	void stopTurn();
	//! Stop moving.
	void stopMove();
	//! Update sensor values.
	void updateSensors();
};

