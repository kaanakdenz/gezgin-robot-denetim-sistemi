
#include "Pose.h"
#include<math.h>

/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

float Pose::getX()
{
	return this->x;
}

void Pose::setX(float x)
{
	this->x = x;
}

float Pose::getY()
{
	return this->y;
}

void Pose::setY(float y)
{
	this->y = y;
}

float Pose::getTh()
{
	return this->th;
}

void Pose::setTh(float th)
{
	this->th = th;
}

bool Pose::operator==(const Pose & other)
{
	return (this->x == other.x&&this->y == other.y&&this->th == other.th);
}

Pose Pose::operator+(const Pose & other)
{
	Pose position;
	position.x = this->x + other.x;
	position.y = this->y + other.y;
	position.th = this->th + other.th;

	return position;
}


Pose Pose::operator-(const Pose & other)
{
	Pose position;
	position.x = this->x - other.x;
	position.y = this->y - other.y;
	position.th = this->th - other.th;

	return position;
}


Pose & Pose::operator+=(const Pose & other)
{
	this->x = this->x + other.x;
	this->y = this->y + other.y;
	this->th = this->th + other.th;

	return *this;
}

Pose & Pose::operator-=(const Pose & other)
{
	this->x = this->x - other.x;
	this->y = this->y - other.y;
	this->th = this->th - other.th;

	return *this;
}

bool Pose::operator<(const Pose & other)
{
	return (this->x < other.x&&this->y < other.y&&this->th < other.th);
}

void Pose::getPose()
{
	cout<<this->getX()<< " ";
	cout<<this->getY() << " ";
	cout<<this->getTh()<<endl;
}

void Pose::setPose(float _x, float _y, float _th)
{
	this->x = _x;
	this->y = _y;
	this->th = _th;

}

float Pose::findDistanceTo(Pose pos)
{
	float xValue = this->x - pos.x;
	if (xValue < 0)
		xValue *= -1;
	float yValue = this->x - pos.x;
	if (yValue < 0)
		yValue *= -1;

	float distance = sqrt(pow(xValue, 2) + pow(yValue, 2));
	return distance;
	
}

float Pose::findAngleTo(Pose pos)
{
	float angle= this->th - pos.th;
	if (angle < 0)
		angle = 360 - angle;

	return angle;

}


