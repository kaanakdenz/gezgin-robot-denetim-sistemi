#ifndef POSE_H_
#define POSE_H_

#include<iostream>
using namespace std;


/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

class Pose
{
private:
	//! x value of robot position.
	float x;
	//! y value of robot position.
	float y;
	//! angle value of robot position.
	float th;

public:
	//! A constructor.
	Pose() {};
	//! A destructor.
	~Pose() {};
	//! Get x value of position.
	float getX();
	//! Set x value of position.
	void setX(float);
	//! Get y value of position.
	float getY();
	//! Set y value of position.
	void setY(float);
	//! Get angle value of position.
	float getTh();
	//! Set angle value of position.
	void setTh(float);
	//! Equalize a position to other.
	bool operator==(const Pose& other);
	//! Add a position to other with new position.
	Pose operator+(const Pose& other);
	//! Remove a position to other with new position.
	Pose operator-(const Pose& other);
	//! Add a position to other.
	Pose& operator+=(const Pose& other);
	//! Remove a position to other.
	Pose& operator-=(const Pose& other);
	//! Compare two positions's value.
	bool operator<(const Pose& other);
	//! Get position.
	void getPose();
	//! Set position.
	void setPose(float _x, float _y, float _th);
	//! Find distance between two position.
	float findDistanceTo(Pose pos);
	//! Find angle between two position.
	float findAngleTo(Pose pos);

};
#endif
