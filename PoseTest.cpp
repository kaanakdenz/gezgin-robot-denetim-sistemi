#include"Pose.h"

using namespace std;

/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

int main() {

	Pose pos1;
	Pose pos2;
	Pose pos3;
	pos1.setX(5);
	pos1.setY(12);
	pos1.setTh(30);
	pos1.getPose();
	pos2.setX(9);	
	pos2.setY(12);
	pos2.setTh(60);
	pos2.getPose();
	pos1 += pos2;
	pos3 = pos1 + pos2;
	pos3.getPose();
	pos1 = pos3 - pos2;
	pos1.getPose();
	cout<<"Pos1's distance to Pos2 is : "<<pos1.findDistanceTo(pos2)<<endl;
	cout << "Pos3's angle to Pos1 is : " << pos3.findAngleTo(pos1)<<endl;
	pos1.setPose(10, 12, 90);
	if (pos2 < pos1)
		cout << "Pos2 is smaller than Pos1" << endl;
	else
		cout << "Pos2 is bigger than Pos1" << endl;
	
	system("pause");
	return 0;

}
