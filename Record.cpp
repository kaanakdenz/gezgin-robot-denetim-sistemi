/**
* @file Record.cpp
* @Author Zhanara Kolbaeva
* @date December 19, 2018
* @brief Record class where it creates file and writes/reads from the file
*/
#include "Record.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
/**
* \brief Constructor for Record class, takes the name of the file from user and calls set function to set name to fileName
* \param name name of the file
*/
Record::Record(string name="no name")
{
	setFileName(name);
	file.open(fileName, ios::in | ios::out | ios::app);
}
/**
* \brief Opens file to read and write from it
* \brief returns true if file is opened or false if file couldn't be opened
*/
bool Record::openFile()
{ 
	file.open(fileName, ios::in | ios::out | ios::app);
	if (!file.is_open())
	{
		cout << "Couldn't open the file\n";
		return false;
	}
	else {
		cout << "File is opened" << endl;
		return true;
	}
}
/**
* \brief Closes file
* \brief Returns true if file is closed or false if file couldn't be closed
*/
bool Record::closeFile()
{
	file.close();
	if (!file.is_open())
	{
		cout << "File is closed" << endl;
		return true;
	}
	else
	{
		cout << "Couldn't close the file" << endl;
		return false;
	}
}
/**
* \brief Function to set det the name of the file 
* \param name name of the file entered by user
*/
void Record::setFileName(string name)
{
	this->fileName = name + ".txt";
}
/**
* \brief Function to read the line from the file
*/
string Record::readLine()
{	
	ifstream file(fileName);
	string str;

	
		getline(file, str);
		cout << str << endl;
	
	return str;
}
/**
* \brief Function to read from file
*/
string Record::read()
{
	ifstream file(fileName);
	string str;
	while (file)
	{
		getline(file, str);
		cout << str << endl;
	}
	return str;
}
/**
* \brief Function to write the line to the file
* \brief return false if it couldn't write to the file
*/
bool Record::writeLine(string str)
{
	if (file.is_open())
	{
		file << str << endl;
	}
	else
	{
		cout << "Unable to open file" << endl;
		return false;
	}
}    
/**
* \brief Overload operator to write to the file
*/
Record & Record::operator<<(string str)
{
	writeLine(str);
	return *this;
}
/**
* \brief Overload operator to read from the file
*/
Record & Record::operator>>(string str)
{
	if (str == this->getFileName())
	{
		read();
	}
	return *this;
}