/**
* @file Record.h
* @Author Zhanara Kolbaeva
* @date December 19, 2018
* @brief Record class where it creates file and writes/reads from the file
*/
#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
/**
* \brief Creating Record class
*/
class Record
{
private:
	string fileName;
	fstream file;
public:
	//!A constructor
	Record(string);
	//!Checks if file is opened or not
	bool openFile();
	//!Checks if file closed or not
	bool closeFile();
	//!Set fucntion for file name
	void setFileName(string);
	//!Reads line from file
	string readLine();
	string read();
	//!Writes line to file
	bool writeLine(string);
	//!Overload operator to write to file
	Record& operator<<(string);
	//!Overload operator to read from file
	Record& operator>>(string);

	string getFileName()const {
		return fileName;
	}
	
};

