#include "Record.h"
#include <iostream>
using namespace std;

int main()
{
	
	string name;
	cout << "Enter file name:";
	cin >> name;
	Record p(name);
	cout << "writing to file...\n";
	p.writeLine( "hey there!");
	p.writeLine("new line!");

	cout << "Reading line from the file" << endl;
	p.readLine();
	p.closeFile();
	cout << endl;

	p.openFile();
	p << "again new line";
	p << "lala lala laaa";
	cout << "Overload operator >> (Reading from file) :\n";
	p.operator>>(p.getFileName());
	p.closeFile();

	system("pause");
	return 0;
}
