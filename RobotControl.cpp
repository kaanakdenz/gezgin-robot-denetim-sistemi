#include "RobotControl.h"
#include<iostream>

/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

using namespace std;

void RobotControl::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void RobotControl::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void RobotControl::forward(float speed)
{
	robotAPI->moveRobot(speed);
}

void RobotControl::backwark(float speed)
{
	robotAPI->moveRobot(-speed);
}

void RobotControl::print()
{
	cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
}

Pose RobotControl::getPose()
{
	return *position;
}

void RobotControl::setPose(Pose pos)
{
	position->setX(pos.getX());
	position->setY(pos.getY());
	position->setTh(pos.getTh());
}

void RobotControl::stopTurn()
{
	robotAPI->stopRobot();
}

void RobotControl::stopMove()
{
	robotAPI->stopRobot();
}
