#ifndef ROBOTCONTROL_H_
#define ROBOTCONTROL_H_

#include"Pose.h"
#include"PioneerRobotAPI.h"
/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

class RobotControl
{

private:
	//! Robot position.
	Pose* position;
	//! Robot.
	PioneerRobotAPI *robotAPI;
public:
	//! RobotControl Constructor. It takes main robot with parameter.
	RobotControl(PioneerRobotAPI *a) {
		this->robotAPI = a;
		position = new Pose();
	};
	//! RobotControl Destructor.
	~RobotControl() {
		delete position;
	};
	//! Turn robot to the left.
	void turnLeft();
	//! Turn robot to the right.
	void turnRight();
	//! Move forward.
	void forward(float speed);
	//! Move backward.
	void backward(float speed);
	//! Print the robot position.
	void print();
	//! Get the position of robot.
	Pose getPose();
	//! Set the position of robot.
	void setPose(Pose);
	//! Stop turning.
	void stopTurn();
	//! Stop moving.
	void stopMove();

};

#endif
