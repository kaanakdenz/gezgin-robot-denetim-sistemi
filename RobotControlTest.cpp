#include<iostream>
#include"RobotControl.h"

using namespace std;

/**
* @file Pose.h 
* @Author Kaan Akdeniz 152120161031
* @date 16/12/2018
* @brief This file using for control of robot position.
*/

int main()
{
	RobotControl *robot = new RobotControl();
	Pose pos;
	pos.setX(5);
	pos.setY(7);
	pos.setTh(30);

	robot->setPose(pos);
	robot->forward(1000);
	robot->turnRight();
	robot->stopTurn();
	robot->backwark(500);
	robot->stopMove();
	robot->turnLeft();
	robot->print();



	system("pause");
}