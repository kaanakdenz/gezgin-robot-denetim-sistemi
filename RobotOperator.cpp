#include <iostream>
#include <string>
#include "RobotOperator.h"

using namespace std;


//Encryption and decryption 
int RobotOperator::callEncrypter(int number) {
	return this->encryptCode(number);
}
int RobotOperator::callDecrypter(int number) {
	return this->decryptCode(number);
}

int RobotOperator::encryptCode(int number) {
	return E.encrypt(number);
}
int RobotOperator::decryptCode(int number) {
	return E.decrypt(number);
}


void RobotOperator::checkAccessCode(int number) {
	
	if (number == getAccessCode()) {
		setAccessState(true);
	}
	else {
		setAccessState(false);
	}
}

//print function
void RobotOperator::print() {
	string name = getName();
	string surname = getSurname();
	bool aState = getAccessState();

	cout << "Name: " << name << endl;
	cout << "Surname: " << surname << endl;
	cout << "Access State " << aState << endl;
}

//getter functions
unsigned int RobotOperator::getAccessCode() {
	return this->accessCode;
}

string RobotOperator::getName() {
	return this->name;
}

string RobotOperator::getSurname() {
	return this->surname;
}

bool RobotOperator::getAccessState() {
	return this->accessState;
}

//setter functions
void RobotOperator::setAccessState(bool a) {
	if (a == true) {
		this->accessState = true;
	}
	else if (a == false) {
		this->accessState = false;
	}
}

void RobotOperator::setName(string _name) {
	name = _name;
}

void RobotOperator::setSurname(string _surname) {
	surname = _surname;
}


