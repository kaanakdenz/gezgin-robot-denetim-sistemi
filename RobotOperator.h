#pragma once
#include<string>
#include "Encryption.h"
using namespace std;
/**
*\brief controls the log in the system for doxygen documentation
*\author Gulnur ALTAN
* detailed takes 4 digit code from the user and encripted
*/
class RobotOperator
{
private:
	//! name value for log in 
	string name;
	//! surname value for log in
	string surname;
	//! access state
	unsigned int accessCode;
	//! access state
	bool accessState;

	Encryption E;

	int encryptCode(int);
	int decryptCode(int);
public:
	RobotOperator(unsigned int _accessCode, bool _state) {
		accessCode = _accessCode;
		accessState = _state;
	}

	void setName(string);

	void setSurname(string);

	void setAccessState(bool);

	//!it does the  access of private variable 
	string getName();

	//!it  does the access of private variable
	string getSurname();

	unsigned int getAccessCode();
	bool getAccessState();

	//! controls the password is equal or not its encrypted version
	void checkAccessCode(int);

	void print();

	//! calls the encryption function 
	int callEncrypter(int);

	//! calls the decryption function
	int callDecrypter(int);
};