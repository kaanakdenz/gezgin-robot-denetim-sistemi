#include <iostream>
#include "RobotOperator.h"
using namespace std;

int main() {
	RobotOperator oper(3412,false);

	oper.setName("gulnur");
	oper.setSurname("altan");

	oper.print();

	cout << endl;
	int encrypted = oper.callEncrypter(4567);

	cout << "Encrypted value :" << encrypted << endl;
	int decrypted = oper.callDecrypter(encrypted);
	cout << "Decrypted value:" << decrypted << endl;
	cout << endl;

	oper.checkAccessCode(encrypted);
	oper.print();
	cout << endl;
	system("pause");
}