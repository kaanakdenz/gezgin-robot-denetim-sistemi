#include "SonarSensor.h"
#include "PioneerRobotAPI.h"
#include <iostream>

SonarSensor::SonarSensor() {}//constructor
SonarSensor::~SonarSensor() {}//destructor function


float SonarSensor::getRange(int index) const
{
	if (0 <= index <= 16) {
		return this->ranges[index];
	}
	else return -1;
}

float SonarSensor::getMax(int &index)
{
	float max = this->ranges[0];
	for (int i = 0; i<16; i++)
	{
		if (this->ranges[i] > max) {
			max = this->ranges[i];
		}
	}
	for (int j = 0; j < 16; j++) {
		if (max == this->ranges[j]) {
			index = j;
		}
	}
	return max;
}

float SonarSensor::getMin(int &index)
{
	float min = this->ranges[0];
	for (int i = 0; i<16; i++)
	{
		if (min>this->ranges[i]) {
			min = this->ranges[i];
		}
	}
	for (int j = 0; j < 16; j++) {
		if (min == this->ranges[j]) {
			index = j;
		}
	}
	return min;
}

void  SonarSensor::updateSensor(float range[])
{
	for (int i = 0; i < 16; i++) {
		this->ranges[i] = range[i];
	}
}

float SonarSensor::operator[](int index)const
{
	if (0 <= index <= 16) {
		return ranges[index];
	}
	else {
		return -1;
	}
}

float SonarSensor::getAngle(int index)
{
	if (index == 0 || index == 8) {
		return 90;
	}
	else if (index == 1 || index == 9) {
		return 50;
	}
	else if (index == 2 || index == 10) {
		return 30;
	}
	else if (index == 3 || index == 11) {
		return 10;
	}
	else if (index == 4 || index == 12) {
		return -10;
	}
	else if (index == 5 || index == 13) {
		return -30;
	}
	else if (index == 6 || index == 14) {
		return -50;
	}
	else if (index == 7 || index == 15) {
		return -90;
	}
	else {
		return -1;
	}
}
