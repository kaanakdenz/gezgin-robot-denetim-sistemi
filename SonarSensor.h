#pragma once
#include "PioneerRobotAPI.h"
#include <iostream>

/**
* @file SonarSensor.h
* @Author �rfan AKARSU
* @brief SonarSensor for recording and showing ranges
*/
class SonarSensor
{
private:
	float ranges[16];
	PioneerRobotAPI *robotAPI;;
public:
	SonarSensor();
	~SonarSensor();

	//! getRange function of SonarSensor, this function finds the range of demanded sensor
	float getRange(int) const;
	//! brief getMax function of SonarSensor, this function finds the maximum range belong sensors, returns maximum range
	//!and pass index of maximum to demanded index value
	float getMax(int&);
	//!getMin function of SonarSensor, this function finds the minimum range belong sensors, returns minimum range
	//!and pass index of minimum to demanded index value
	float getMin(int&);
	//!updateSensor function of SonarSensor, this function updates sensor values by demanded vales's array
	void updateSensor(float[]);
	//!brief operator[] function of SonarSensor, this function overrides [] operator for returning value of demanded sensor
	float operator[](int) const;
	//!getAngle function of SonarSensor, this function finds angle of demanded sensor
	float getAngle(int);
};

